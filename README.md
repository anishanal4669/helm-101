# helm-101


## Recap


A deployment manifest in Kubernetes is a blueprint that describes how to deploy and manage containerized applications. It's essentially a text file written in YAML format that specifies the desired state of your deployment, including:

 - **Number of replicas**: This defines how many instances of your application pod (a group of containers) you want running at any given time.
 - **Container image**: The manifest specifies the Docker image that contains your application code and dependencies.
 - **Resources**: You can define resource requirements like CPU and memory for your pods.
 - **Networking**: The manifest can configure how your pods expose services and communicate with each other.


an initContainer is a special type of container that runs to completion before the main application containers in a pod are started. It acts like a preparatory step, allowing you to perform tasks like initialization, configuration, or setting up dependencies that your application needs before it starts.

 

A ConfigMap manifest in Kubernetes is a recipe for creating a ConfigMap object. This object acts like a key-value store for storing non-confidential configuration data for your application. 




A persistent volume (PV) manifest in Kubernetes is a configuration file, typically in YAML format, that describes a unit of storage available to your pods. It acts like a virtual disk that persists data even when the pod restarts or is rescheduled to a different node in the cluster.

A persistent volume claim (PVC) manifest is a configuration file, usually in YAML format, that describes a request for storage from an application. It acts like a shopping list for persistent storage, telling Kubernetes what kind of storage your application needs.



## Helm Basics

Have you ever been confused about how to deploy applications to Kubernetes? That's where Helm comes in! Helm is a package manager for Kubernetes that simplifies the deployment and management of applications. By using Helm, you can package all the Kubernetes resources required to run an application into a single chart, making it easy to deploy and manage applications across different environments.

In short, Helm Charts help you define, install, and upgrade even the most complex Kubernetes application.

Now, Why shall we use helm what would be benefit that we will get ?

Let’s assume you have four different environments in your project. Dev, QA
, Staging, and Prod. Each environment will have different parameters for Nginx deployment. For example,

In Dev and QA you might need only one replica.
In staging and production, you will have more replicas with pod autoscaling.
The ingress routing rules will be different in each environment.
The config and secrets will be different for each environment.


Because of the change in configs and deployment parameters for each environment, you need to maintain different application deployment files for each environment. Or you will have a single deployment file and you will need to write custom scripts to replace values based on the environment. But that would notbe a scalable approach. And, Here is where the helm chart comes into the picture.


## Helm Chart structure

 - **charts**: In this directory, We can add another chart’s structure inside this directory if our main charts have some dependency on others. By default this directory is empty.

 - **Chart.yaml**: It contains information about the helm chart like version, name, description, etc.

 - **values.yaml**: In this file, we define the values for the YAML templates. For example, image name, replica count, HPA values, etc. Also, you can override these values dynamically or at the time of installing the chart using --values or --set command.

 - **templates**: This directory contains all the Kubernetes manifest files that form an application. These manifest files can be templated to access values from values.yaml file. Helm creates some default templates for Kubernetes objects like deployment.yaml, service.yaml etc, which we can use directly, modify, or override with our files.

  - **.helmignore**: It is used to define all the files that we don’t want to include in the helm chart. It works similarly to the .gitignore file.

 - **templates/NOTES.txt**: This is a plaintext file that gets printed out after the chart is successfully deployed. 

 - **templates/_helpers.tpl**: That file contains several methods and sub-template. These files are not rendered to Kubernetes object definitions but are available everywhere within other chart templates for use.

 - **templates/tests/**: We can define tests in our charts to validate that your chart works as expected when it is installed.

 Now, Let's create a helm chart on DCD.

 helm create sample-app 



## Helm Templates

The idea of a helm chart is to template the YAML files so that we can reuse them in multiple environments by dynamically assigning values to them.

To template a value, all you need to do is add the object parameter inside curly braces as shown below. It is called a template directive and the syntax is specific to the Go templating
{{ .Object.Parameter }}


First Let’s understand what is an Object. Following are the three Objects we are going to use in this example.

 - **Release**: Every helm chart will be deployed with a release name. If you want to use the release name or access release-related dynamic values inside the template, you can use the release object.
 - **Chart**: If you want to use any values you mentioned in the chart.yaml, you can use the chart object.
 - **Values**: All parameters inside values.yaml file can be accessed using the Values object.


## Alignment of Helm Chart for deployment

